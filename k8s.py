import subprocess
from time import gmtime, strftime
from os import listdir
from os.path import isfile, join

class K8s:

    def apply_folder(self, folder):      
        files = self.__get_files(folder) 
        print("Files to apply : " + str(files))
        self.__apply(folder, files)

    def __get_files(self,folder):
        return [f for f in listdir(folder) if isfile(join(folder, f))]
    
    def __as_comma_separated(self,folder,files):
        folder_slash = "{folder}/".format(folder = folder)
        comma_folder_slash = ',' + folder_slash
        return folder_slash + comma_folder_slash.join(files)

    def __apply(self,folder,files):
        yaml_list = self.__as_comma_separated(folder,files)
        subprocess.run(["kubectl", "apply", "-f", yaml_list])
