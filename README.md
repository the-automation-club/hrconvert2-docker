# HRConvert2-Docker

<img src="icon.png" width="128" height="128" alt="hrconvert2 Logo" />

[Convert icons created by iconixar - Flaticon](https://www.flaticon.com/free-icons/convert)

Docker files for HRConvert2 from https://github.com/zelon88/HRConvert2 - A self-hosted, drag-and-drop, & nosql file conversion server that supports 62x file formats (forked from https://github.com/dwaaan/HRConvert2-Docker)

## Building the image yourself

Select the relese to build from the [release list](https://github.com/zelon88/HRConvert2/releases), run `./create-image <release>` and a local image tagged **hrconvert2:*release*** will be generated. Can be used locally (`docker run hrconvert2:<release>`) or upload to an image registry ([Docker Hub](https://docs.docker.com/docker-hub/), [Gitlab](https://docs.gitlab.com/ee/user/packages/container_registry/) or [any other](https://www.slant.co/topics/2436/~docker-image-private-registries)) adding the `--publish` switch

In case `./create-image latest` is used the latest code from repository will be used and the local image name becames `hrconvert2:yyyymmdd` being **yyyymmdd** today year, month and date

For simultaneous cross build platforms add `--platforms` with a list of comma separated platforms to build and all is done

Changing the image name is easy as adding `--name`. Mandatory when publishing to an image registry (prepend your username)

### Examples

Build local image using docker

```shell
./create-image <release>
```

Build image using docker and publish it

```shell
./create-image --publish --name bitman09/hrconvert2 <release>
```

Build image using dockerx and publish it

```shell
./create-image --publish  --platforms linux/amd64,linux/arm64 <release>
```

### Testing changes

Is a good practice to test your changes before publishing them but how do you test a change in HRConvert2 code when it's not yet merged?

To accomplish this task create a git clone of `zelon88/HRConvert2`, make the changes and test them against a new docker image by using the cloned repo instead of HRConvert2 (use `--git`)

```shell
./create-image  --git https://cloned/repo/with-fix latest
```

Once the fix is verified against `hrconvert2:<YYYMMDD>`  local image crete a pull request against `zelon88/HRConvert2` git repository

## Deploying to kubernetes

Easy as pie. Prerequisites:

* Access to kubernetes cluster from your desktop computer
* kubectl command installed and accessing kubernetes cluster
* Python 3.4+

Modifiy the IP address to access HRConvert from outside the cluster (file `k8s/service.yml` , `externalIPs`) and run this command to install HRConvert2 into your k8s cluster

```shell
./install_k8s
```

Voilà!
