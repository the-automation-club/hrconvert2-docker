# Changelog

### April 28, 2024

* Create docker image from a cloned HRConvert2 project

### April 1, 2024

* Dockerfile now at HRConvert2 original project
* Integrated buildx option
* Allows publishing

### March 10, 2024

* Easier command execution
* fix rc.local location

### December 28, 2023

* Local image generation script

### May 10, 2022

* updated base image to ubuntu 20.04
* updated php from 7.2 to 7.4
* updated HRConvert2 from 2.6 to 2.9.2
