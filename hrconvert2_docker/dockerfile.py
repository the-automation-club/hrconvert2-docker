import urllib.request
from tempfile import mkstemp
from os import fdopen, remove
from shutil import move, copymode

class Dockerfile:
    
    def __init__(self, temp_folder_name):
        self.__temp_folder_name = temp_folder_name

    ## https://stackoverflow.com/a/39110/1085978
    def __replace(self, file_path, pattern, subst):
        fh, abs_path = mkstemp()
        with fdopen(fh,'w') as new_file:
            with open(file_path) as old_file:
                for line in old_file:
                    new_file.write(line.replace(pattern, subst))

        copymode(file_path, abs_path)
        remove(file_path)
        move(abs_path, file_path)

    def get(self, release, git_repo):
        url = "https://github.com/zelon88/HRConvert2/raw/{release}/Documentation/Build/Dockerfile".format(release = release)
        download_to = "{tmp}/Dockerfile".format(tmp = self.__temp_folder_name)
        urllib.request.urlretrieve(url, download_to)
        self.__replace(download_to, 'https://github.com/zelon88/HRConvert2', git_repo)
        return self.__temp_folder_name