import os

class TempFolder:

    def __init__(self, base_folder):
        self.path = os.path.join(base_folder, "temp")

    def create(self):
        if not os.path.exists(self.path):
            os.makedirs(self.path)