class Release:
    def __init__(self, id, is_tag):
        self.id = id
        self.is_tag = is_tag

    def __str__(self):
        if self.is_tag:
            return "Release {r}".format(r = self.id)
        return "Branch {b}".format(b = self.id)
